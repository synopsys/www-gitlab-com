---
layout: handbook-page-toc
title: "Learn How to Use Chorus.ai"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Confused about how to use Chorus? Check out the links below for more information on how to get the most out of this important sales tool.

## How to Remove Chorus.ai From a Meeting

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.loom.com/share/a6513ac235ae4eb9acaaeb167d7583ce" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->

## Chorus.ai Basics

- [How to Remove Chorus From a Meeting](https://www.loom.com/share/a6513ac235ae4eb9acaaeb167d7583ce) 
- [Getting Started with Chorus](https://docs.chorus.ai/hc/en-us/sections/115002365608-Getting-Started-with-Chorus)
- [Chorus FAQs](https://docs.chorus.ai/hc/en-us/sections/115002365588-FAQs)
- [Chorus Basics for SDRs and Reps](https://docs.chorus.ai/hc/en-us/sections/360003251593-Chorus-Basics-for-SDRs-BDRs-and-Reps)
- [Chorus Basics for Managers & Sales Enablement](https://docs.chorus.ai/hc/en-us/sections/115002370787-Chorus-Basics-for-Managers-Sales-Enablement)
- [What’s the maximum length of a Chorus recorded meeting?](https://docs.chorus.ai/hc/en-us/articles/360045702734-What-s-the-maximum-length-of-a-Chorus-recorded-meeting-)

## Security

- Any time a Chorus link is shared, a password should be required or the link should be set to expire. Each user has to do this; it can not be turned on globally from the admin panel.
As a best practice, please DO NOT include links to Chorus recordings in a public issue. If you need to include a Chorus recording or any type of customer information in an issue, please first mark the issue as `Confidential`.
For additional details, please see [this issue](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/-/issues/1560#note_702890753).


## Chorus.ai Advanced Usage

- [Chorus Integrations](https://docs.chorus.ai/hc/en-us/sections/115002215568-Integrations)
- [Compliance](https://docs.chorus.ai/hc/en-us/sections/360001251353-Compliance)

## Why is meeting live on chorus?
- Zoom and Chorus have gone through a lot of legal and development cycles to be transparent in what data is being used for all participants, so ihe verbiage cannot be customized at this time. 
- They (Chorus and Zoom) do see some areas internally where this is under further review, so it is possible Zoom/Chorus could work to update the message at some point. 

## Recording with Breakout Rooms
- Chorus will not capture the breakout rooms, only the main zoom meeting will be captured.
- Please note: By default Chorus has some rules around silence on a call. So if participants are put into breakout rooms for longer than 5 minutes in the middle of the call, Chorus will drop out of the call due to silence thinking the meeting is over
