---
layout: markdown_page
title: "Category Direction - Kubernetes Management"
description: "GitLab aim to provide a simple way for users to configure their clusters and manage Kubernetes. Learn more here!"
canonical_path: "/direction/configure/kubernetes_management/"
---

- TOC
{:toc}

## Overview

The main motivating factor behind [using a container orchestration platform is cost reduction](https://medium.com/kubernetes-tutorials/how-can-containers-and-kubernetes-save-you-money-fc66b0c94022). Kubernetes has become the most widely adopted container orchestration platform to run, deploy, and manage applications. Yet, to seamlessly connect application code with the clusters in various environments, has remained a complex task that needs various approaches of CI/CD and Infrastructure as Code. 

Our mission is to support the Platform Engineers and SRE in enabling developer workflows, to make deployments to every environment frictionless for the developer and reliable for the operator, no matter the experience level. We do this by supporting an integrated experience within GitLab and leverage the existing tools and approaches in the Kubernetes community.

### Market overview

Kubernetes is emerging as the _de facto_ standard for container orchestration. Today, Kubernetes runs in half of container environments, and around 90% of containers are orchestrated. ([Source](https://www.datadoghq.com/container-report/)).

### How can you contribute

Interested in joining the conversation for this category? Please have a look at our [global epic](https://gitlab.com/groups/gitlab-org/-/epics/115) where
we discuss this topic and can answer any questions you may have or direct your attention to one of the more targeted epics presented below where our focus is today.
Your contributions are more than welcome.

## Vision

By 2022, GitLab is an indispensable addition in the workflow of Kubernetes users by making deployments reliable, frictionless and conventional, and by surfacing in-cluster insights in the form of alerts, metrics and logs in GitLab. As we are primarily focusing on enterprise customers, we want to support highly regulated, air-gapped use cases along with generic, public-cloud based environments.

## Strategy

We invite you to [share your feedback and join the discussion](https://gitlab.com/gitlab-org/gitlab/-/issues/342696) on the future of our Kubernetes integrations.

### Pricing strategy

The Kubernetes Agent is aimed at Platform and SRE teams. As a result, we assume that there are other teams our users serve. For this reason, our pricing leans heavily towards the Premium tier. Around every use case, we plan to launch as a Premium product, and want to move parts of the feature to Core as we better understand our users. The dividing line between Core and Premium features until now is around permissions management.

Example: We moved the CI/CD tunnel to GitLab Core, but only when the Agent uses the service account of the cluster side component. At the same time, we are building out a set of "impersonating" featues, where a GitLab user or a CI/CD job can be targeted with Kubernetes RBAC rules. The latter feature set will remain in Premium.

### Today

The recommended approach to connect a cluster with GitLab is via the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/). The legacy, certificate based connection is currently being deprecated. Beside these, one can manually configure a `$KUBECONFIG` or use 3rd party tooling around GitLab CI or webhooks. You can read more about the deprecated features in [the related epic](https://gitlab.com/groups/gitlab-org/configure/-/epics/8).

Using the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/), we want to offer a security oriented, Kubernetes-friendly integration option to Platform Engineers. Beside providing more control for the cluster operator, the agent opens up many new possibilities to make GitLab - cluster integrations easier and deeper. We expect the agent to support various GitLab features with automatic setups and integrations.

As the GitLab Kubernetes Agent in itself does not have any features and is responsible for the cluster connection only, every team is welcome to build on this connection. The Agent provides a bi-directional communication channel between the cluster and GitLab, as a result either GitLab can reach out into the cluster, or the cluster side component can message GitLab. The Agent is available from GitLab CI/CD as well using the CI/CD tunnel.

Today, the Agent supports the following use cases:

- pull based deployments built on top of the `cli-utils` from Google
- push based deployments using the GitLab Runners, this provides support for many other features, like Auto Deploy
- network security alert integrations with Cillium 

### Next 6 months

As we deprecate the certificate based integrations, we want to make sure that we can offer alternatives for the most valuable parts of that integration. These are

- Auto DevOps (especially Auto Deploy)
- GitLab Managed Clusters
- Minimal observability in the form of Deployment boards

Beside the feature development, we are actively [rewriting and extending the related documentation](https://gitlab.com/groups/gitlab-org/-/epics/6267) to be focused around the GitLab Kubernetes Agent.

Together with the above, we are working on improving the user interfaces around Agent connections, and the "impersonation" features mentioned under the pricing strategy.

### Next 9-12 months

We want to make our integrations easy to use. This might mean more automation, conventions and UX improvements. We consider this especially important when we target Software Developers with our features, who might not be familiar with Kubernetes or GitLab at all.

We want to improve the observability aspects of our integrations.

## Target User

We are building GitLab's Kubernetes Management category for the enterprise operators, who are already operating production grade clusters and work hard to remove all the roadblocks that could slow down developers without putting operations at risk. Our secondary persona is the Application Operator, who is in charge of 2nd day operations of deployed applictions in production environments.

- [Priyanka, the platform engineer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)
- [Allison, the application operator](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#allison-application-ops)

### Jobs

<%= partial("direction/jtbd-list", locals: { stage_key: "Configure_K8s" }) %>

## Roadmap

A more detailed view into [our roadmap is maintained within GitLab](https://gitlab.com/groups/gitlab-org/-/roadmap?state=opened&sort=end_date_asc&label_name%5B%5D=group%3A%3Aconfigure&label_name%5B%5D=Roadmap).

## Challenges

We see Kubernetes as a platform for delivering containerised software to users. Currently, every company builds their own workflows on top of it. The biggest challenges as we see them are the following:

- Making Kubernetes developer friendly is hard. We want to make deployments to every Kubernetes environment to be frictionless for the developer and reliable for the operator.
- As clusters are complex, having an overview of the state and contents of a cluster is hard. We want to help our [monitoring efforts](/direction/monitor/) to provide clean visualisations and a rich understanding of one's cluster.

## Approach

- Kubernetes has a large and engaged community, and we want to build on the knowledge and wisdom of the community, instead of re-inventing existing solutions.
- In Kubernetes there are a plethora of ways for achieving a given goal. We want to provide a default setup and configuration options to integrate with popular approaches and tools.

## Maturity

Kubernetes Management is currently `Viable`.

## Competitive landscape

### Argo CD

Argo CD is often considered to be the leading GitOps tool for Kubernetes with an outstanding UI.

### IBM Cloud Native Toolkit

It provides an Open Source Software Development Life Cycle (SDLC) and complements IBM Cloud Pak solutions. The Cloud Native Toolkit enables application development teams to deliver business value quickly using Red Hat OpenShift and Kubernetes on IBM Cloud.

### Spinnaker

Spinnaker is an open-source, multi-cloud continuous delivery platform that helps you release software changes with high velocity and confidence. It provides two core sets of features 1) Application management, and 2) Application deployment.

### Backstage

Backstage.io with its service registry oriented approach and kubernetes integration provides an umbrella to integrate other DevOps tools and provides some insights required by developers of Kubernetes workloads.

## Analyst landscape

This category doesn't quite fit the "configuration management" area as it relates only to Kubernetes. No analyst area currently defined. 

## Top Customer Success/Sales issue(s)

[Customize Kubernetes namespace per environment for managed clusters](https://gitlab.com/gitlab-org/gitlab/-/issues/38054)

## Top user issue(s)

- [Investigate a recommended way to set up GitLab Integrated Applications](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/104)
- [Simplify getting started with the Agent](https://gitlab.com/groups/gitlab-org/-/epics/5786)

## Top internal customer issue(s)

## Top Vision Item(s)

- [Kubernetes Management - enterprise level K8s integration](https://gitlab.com/gitlab-org/gitlab/-/issues/216569)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)
